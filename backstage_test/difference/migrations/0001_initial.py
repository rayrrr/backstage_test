# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='CalculatedValue',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('datetime', models.DateTimeField(auto_now=True)),
                ('value', models.IntegerField()),
                ('number', models.IntegerField()),
                ('occurrences', models.IntegerField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
