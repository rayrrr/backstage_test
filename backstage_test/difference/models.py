# Ray Riga Backstage Test

from django.db import models

class CalculatedValue(models.Model):

	datetime = models.DateTimeField(auto_now=True) # 'modified' timestamp
	value = models.IntegerField() # solution
	number = models.IntegerField() # n
	occurrences = models.IntegerField()
