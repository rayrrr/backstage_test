# Ray Riga Backstage Test

from django.http import JsonResponse
from difference.models import CalculatedValue
from django.core.exceptions import ObjectDoesNotExist

def calculate(n):
	"""Difference Engine (not Babbage's)

		Calculates the difference between squares of sums
		and sums of squares, returns the info dict, 
		and records the lookup
	"""

	# see if we already calculated this one
	try:
		calcVal = CalculatedValue.objects.get(number=n)
		calcVal.occurrences += 1
		calcVal.save()
	except ObjectDoesNotExist:
		calcVal = CalculatedValue()
		calcVal.value = (sum(range(1,n+1)))**2 - sum((x**2 for x in range(1,n+1)))
		calcVal.number = n
		calcVal.occurrences = 1
		calcVal.save()

	return {'datetime': calcVal.datetime, 
			'value': calcVal.value,
			'number': n,
			'occurrences': calcVal.occurrences
			}

def difference(request):
	"""Difference between sum of squares and square of sums
	
	Given a natural number n < 100, return the difference
	between the sum of the squares and the sqaure of the sums
	in the sequence from 1..n
	"""
	responseMsg = {}

	if 'number' in request.GET:
		# check that we have a natural number less than 101
		try:
			n = int(request.GET['number'])
			if n < 1 or n > 100:
				responseMsg['error'] = 'Please enter a number between 1 and 100.'
			else:
				# do the calc
				responseMsg = calculate(n)
		except ValueError, e:
			responseMsg['error'] = 'Please enter a valid natural number between 1 and 100. %s' % e
	else:
		responseMsg['error'] = 'No data supplied. Please try again.'
		
	return JsonResponse(responseMsg)