# README #

This is Ray Riga's response to the Backstage challenge, the difference between sums of squares and squares of sums.

### What is this repository for? ###

* Django/MySQL Persistent Difference Engine
* index.html with jQuery

### How do I get set up? ###

Firstly, set up a MySQL database. in MySQL console:

> CREATE DATABASE 'riga_backstage_test';

> CREATE USER 'riga_backstage'@'localhost' IDENTIFIED BY '98YF397Hs9uh3UHRo923898&#R&HF@^@#TV';

> GRANT ALL ON riga_backstage_test.* TO 'riga_backstage'@'localhost';

Then `pip install -r requirements.txt`, `cd` to the project directory, and `python manage.py migrate`. Then you can `python manage.py runserver` and open the index.html file in the root dir of the repo to try it out!